### Local run

* Run command `docker-compose up;docker-compose down`
* Navigate to [localhost:8080](http://localhost:8080)

### Openshift deployment memo:

1. Create postgresql database from catalog
    * memory limit 8G
    * add PostgreSQL Connection Username
    * add PostgreSQL Connection Password
    * change PostgreSQL Database Name to 'nextcloud'
    * change Volume Capacity to 4Gi
    * click next
    * Select Create a secret in ... and click create
1. Create shared storage with name nextcloud and shared read write access
    * user the system created secrets
1. Add nextcloud secrets
    * Admin and admins password
1. Import nextcloud-configmap.yaml
1. Import app-imagestream.yaml
1. Import app-deploymentconfig.yaml
1. Import web-service.yaml
1. Import web-imagestream.yaml
1. Import web-deploymentconfig.yaml
1. go to directory nginx
1. Run command `oc new-build --to=web:latest --name=web --binary`
1. Run command `oc start-build web --from-dir=./ --follow`
1. Go to directory nextcloud
1. Run command `oc new-build --to=app:latest --name=app --binary`
1. Add to [build environment](https://rahti.csc.fi:8443/console/project/nextcloud-test/browse/builds/app?tab=environment) REDIS_HOST & REDIS_HOST_PASSWORD
1. Run command `oc start-build app --from-dir=./ --follow`
1. Add a route to the web services port 8080
1. Disable apps from the admin ui (at least deleted & versions apps)
1. Import app-cron-job.yaml

### Upgrading

1. Change the FROM line in the nextcloud/Docker
1. Change the version in app-cron-job.yaml to match
1. Go to nextcloud directory
1. Run command `oc new-build --to=app:latest --name=app --binary` 
1. Delete the app-cron-job
1. import the cron file

### Changing volume

1. Mount volume to the pod
1. Sync files rsync -avzh ./html/* <target>
1. Unmount and mount the new volume
